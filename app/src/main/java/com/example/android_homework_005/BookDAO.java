package com.example.android_homework_005;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface BookDAO {
    @Query("Select * From tblBook Order by id desc ")
    List<Book> getBook();

    @Query("SELECT * FROM tblBook WHERE id=:id")
    Book findBookById(int id);

    @Insert
    void insertBook(Book... book);
}
