package com.example.android_homework_005;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements MyAlertDialog.OnBookListener {
    FragmentManager manager;
    FragmentTransaction transaction;
    TopFragment topFragment;
    private ContentFragment contentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contentFragment = new ContentFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.book_content,contentFragment )
                .addToBackStack("Main")
                .commit();

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if(menuItem.getItemId()== R.id.home_tab)
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.book_content,new ContentFragment())
                            .addToBackStack("Main")
                            .commit();
                return true;
            }
        });

    }


    @Override
    public void onInsertBook(Book book) {

        Log.e("BOOK", book.toString());
        BookDAO bookDAO = BookDatabase.getInstanceBook(this).getBookDao();
        bookDAO.insertBook(book);
        contentFragment.onInsertBook(book);
    }
}
