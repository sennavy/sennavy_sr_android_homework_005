package com.example.android_homework_005;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.DialogFragment;
import androidx.room.Database;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;
import java.util.Objects;

public class MyAlertDialog extends DialogFragment {
    private ImageView imageView;
    private FloatingActionButton floatingActionButton;
    private final int GALLERY_REQUEST_CODE = 1;
    private EditText txtTitle;
    private EditText txtSize;
    private EditText txtPrice;
    private Spinner spnCategory;
    private ImageView image;
    private TextView btnInsert;
    private TextView btnCancelInsert;
    private OnBookListener onBookListener;
    Uri selectedImage;

    public MyAlertDialog() {
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.alert_dialog,container,false);

        floatingActionButton = view.findViewById(R.id.btn_pick_image);
        imageView = view.findViewById(R.id.book_cover);
        btnCancelInsert = view.findViewById(R.id.btn_cancel_insert);
        btnInsert = view.findViewById(R.id.btn_insert);

        txtTitle = view.findViewById(R.id.book_title);
        txtSize = view.findViewById(R.id.size);
        txtPrice = view.findViewById(R.id.price);
        spnCategory = view.findViewById(R.id.category);
        image = view.findViewById(R.id.book_cover);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        //pick image
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");
                startActivityForResult(pickIntent,1);
            }
        });

        //cancel button
        btnCancelInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_SHORT).show();
                Objects.requireNonNull(getDialog()).dismiss();
            }
        });

        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = new Book(
                        0,
                        txtTitle.getText().toString(),
                        spnCategory.getSelectedItem().toString(),
                        Integer.parseInt(txtSize.getText().toString()),
                        Double.parseDouble(txtPrice.getText().toString()),
                        selectedImage.toString()


                );
                Log.e("BOOK", "onClick: "+book.toString());
                onBookListener.onInsertBook(book);
                Objects.requireNonNull(getDialog()).dismiss();

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //get image
        if(requestCode == 1 && resultCode == Activity.RESULT_OK && null != data){
            selectedImage = data.getData();
            imageView.setImageURI(Uri.parse(selectedImage + ""));
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        Objects.requireNonNull(Objects.requireNonNull(getDialog()).getWindow()).setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof OnBookListener) {
            onBookListener = (OnBookListener) context;
        } else {
            onBookListener = null;
        }
    }

    interface OnBookListener {
        void onInsertBook(Book book);
    }
}
