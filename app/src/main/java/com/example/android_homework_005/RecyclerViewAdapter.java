package com.example.android_homework_005;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private Context mContext;
    private List<Book> bookList;

    public RecyclerViewAdapter(Context context, List<Book> bookList) {
        this.mContext = context;
        this.bookList = bookList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.book_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Book book = bookList.get(position);
        holder.title.setText(book.getTitle());
        holder.category.setText(book.getCategory());
        holder.size.setText(book.getSize()+"");
        holder.price.setText( book.getPrice() + "");


        Log.e("BOOK", Uri.parse(book.getImage()) + "");
        if(book.getImage() == null){
            holder.image.setImageResource(R.drawable.dark_life);
        }else {
//            holder.image.setImageURI(Uri.parse(book.getImage()));
        }

        holder.option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, holder.option);
                popupMenu.inflate(R.menu.menu_item);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.remove:
                                Toast.makeText(mContext, "Book has been removed", Toast.LENGTH_SHORT).show();
                                holder.removeBookAlert(v,1);
                                break;
                            case R.id.edit:
                                Toast.makeText(mContext, "Updated", Toast.LENGTH_SHORT).show();
                            case R.id.read:
                                Toast.makeText(mContext, "Read", Toast.LENGTH_SHORT).show();
                                v.getContext().startActivity(new Intent(v.getContext(),ReadBook.class));
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public void onInsertBook(Book book) {
        bookList.add(0, book);
        notifyItemInserted(0);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView cover;
        TextView title;
        TextView category;
        TextView price;
        TextView size;
        TextView option;
        ImageView image;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.book_title);
            category = itemView.findViewById(R.id.book_category);
            size = itemView.findViewById(R.id.book_size);
            price = itemView.findViewById(R.id.book_price);
            option = itemView.findViewById(R.id.btn_option);
            image = itemView.findViewById(R.id.book_cover);

        }
        private void removeBookAlert(View view, int id){
            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
            builder.setMessage("Are you sure to remove?")
                    .setTitle("Remove Book");

            // confirm delete button
            builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            // cancel delete button
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }
}
