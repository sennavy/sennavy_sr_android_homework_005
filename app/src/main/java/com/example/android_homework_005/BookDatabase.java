package com.example.android_homework_005;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Book.class},version = 1,exportSchema = false)
public abstract class BookDatabase extends RoomDatabase {
    public abstract BookDAO getBookDao();

    public static BookDatabase getInstanceBook(Context context) {
        return Room.databaseBuilder(context.getApplicationContext(), BookDatabase.class, "book_db")
                .allowMainThreadQueries()
                .build();
    }
}
