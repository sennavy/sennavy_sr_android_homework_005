package com.example.android_homework_005;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ContentFragment extends Fragment {

    RecyclerView recyclerView;
    List<Book> bookList;
    private RecyclerViewAdapter recyclerViewAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_content_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycler_view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        bookList = new ArrayList<>();
        loadRecyclerViewItem();
    }

    private void loadRecyclerViewItem() {

        BookDAO bookDAO = BookDatabase.getInstanceBook(getActivity()).getBookDao();
        bookList = bookDAO.getBook();
        recyclerViewAdapter = new RecyclerViewAdapter(getActivity(),bookList);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    public void onInsertBook (Book book) {
        recyclerViewAdapter.onInsertBook(book);
    }
}
